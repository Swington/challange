import React from "react";

interface SpreadsheetsSelectableListProps {
    spreadsheetsIds: string[]
    onSelectChange: Function
}

export class SpreadsheetsSelectableList extends React.Component<SpreadsheetsSelectableListProps> {
    handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        this.props.onSelectChange(event)
    }

    render() {
        let spreadsheets = this.props.spreadsheetsIds;
        let options = spreadsheets.map((spreadsheet_id) => <option key={spreadsheet_id} value={spreadsheet_id}> {spreadsheet_id} </option>);
        return (
            <select
                name="customSearch"
                className="custom-search-select"
                onChange={this.handleChange}
            >
            <option>Select spreadsheet</option>
            {options}
            </select>)
    }
}
