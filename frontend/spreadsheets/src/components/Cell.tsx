import React from "react";

type CellProps = {
    onChangedValue: Function
    y: number
    x: number
    id: string
    value: string
    expression: string
}

type CellState = {
    selected: boolean
    editing: boolean
    value: string
    expression: string
}

export const ALPHABET = ' abcdefghijklmnopqrstuvwxyz'.split('');

export class Cell extends React.Component<CellProps, CellState> {
    display: string
    timer: number
    delay: number
    prevent: boolean

    constructor(props: CellProps) {
        super(props)
        this.state = {
            editing: false,
            selected: false,
            value: props.value,
            expression: props.expression
        }
        this.display = this.determineDisplay(props.value)
        this.timer = 0
        this.delay = 200
        this.prevent = false
    }

    componentDidMount() {
        window.document.addEventListener('unselectAll', this.handleUnselectAll)
    }

    componentWillUpdate() {
        this.display = this.determineDisplay(this.state.value)
    }

    componentWillUnmount() {
        window.document.removeEventListener('unselectAll', this.handleUnselectAll)
    }

    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            expression: e.target.value
        })
        this.display = this.determineDisplay(e.target.value)
    }

    onKeyPressOnInput = (e: any) => {
        if (e.key === "Enter") {
            this.hasNewValue(this.props.id, e.target.value || "")
        }
    }

    onKeyPressOnSpan = () => {
        if (!this.state.editing) {
            this.setState({editing: true})
        }
        this.display = this.determineDisplay(this.props.value)
    }

    handleUnselectAll = () => {
        if (this.state.selected || this.state.editing) {
            this.setState({selected: false, editing: false})
        }
    }

    hasNewValue = (id: string, value: string) => {
        this.props.onChangedValue(
            id,
            value,
        )
        this.setState({editing: false})
    }

    emitUnselectAllEvent = () => {
        const unselectAllEvent = new Event('unselectAll')
        window.document.dispatchEvent(unselectAllEvent)
    }

    clicked = () => {
        clearTimeout(this.timer)
        this.prevent = true

        this.emitUnselectAllEvent()
        this.setState({editing: true, selected: true})
    }

    determineDisplay = (value: string) => {
        return value
    }

    calculateCss = () => {
        const css: any = {
            width: '80px',
            padding: '4px',
            margin: '0',
            height: '25px',
            boxSizing: 'border-box',
            position: 'relative',
            display: 'inline-block',
            color: 'black',
            border: '1px solid #cacaca',
            textAlign: 'left',
            verticalAlign: 'top',
            fontSize: '14px',
            lineHeight: '15px',
            overflow: 'hidden',
            fontFamily: 'Calibri, \'Segoe UI\', Thonburi, Arial, Verdana, sans-serif',
        }

        if (this.props.x === 0 || this.props.y === 0) {
            css.textAlign = 'center'
            css.backgroundColor = '#f0f0f0'
            css.fontWeight = 'bold'
        }

        return css
    }

    render() {
        const css = this.calculateCss()

        if (this.props.x === 0) {
            return (
                <span style={css}>
                    {this.props.y}
                </span>
            )
        }

        if (this.props.y === 0) {
            return (
                <span onKeyPress={this.onKeyPressOnSpan} style={css} role="presentation">
                    {ALPHABET[this.props.x]}
                </span>
            )
        }

        if (this.state.editing) {
            return (
                <input
                    style={css}
                    type="text"
                    onKeyPress={this.onKeyPressOnInput}
                    value={this.state.expression || this.props.expression}
                    onChange={this.onChange}
                    autoFocus
                />
            )
        }
        return (
            <span
                onClick={e => this.clicked()}
                style={css}
                role="presentation"
            >
                {this.determineDisplay(this.props.value)}
            </span>
        )
    }
}