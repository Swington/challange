import React from "react";
import {Row} from "./Row";
import {CellDTO} from "../api/spreadsheets";

export const ALPHABET = ' abcdefghijklmnopqrstuvwxyz'.toUpperCase().split('');


interface TableProps {
    numberOfRows: number
    numberOfColumns: number
    cells: any
    handleChangedCell: Function
}

type TableState = {
    cells: any
}

export class Table extends React.Component<TableProps, TableState> {
    constructor(props: TableProps) {
        super(props)

        this.state = {
            cells: props.cells,
        }
    }

    render() {
        const rows: any[] = []

        for (let rowNo = 0; rowNo < this.props.numberOfRows; rowNo++) {
            let rowData = new Map<string, CellDTO>()
            for (const key in this.props.cells) {
                if (key.includes(rowNo.toString())) {
                    rowData.set(key, this.props.cells[key] || {value: "", expression: ""})
                }
            }
            rows.push(
                <Row
                    handleChangedCell={this.props.handleChangedCell}
                    key={rowNo}
                    rowNo={rowNo}
                    numberOfColumns={this.props.numberOfColumns + 1}
                    rowData={rowData}
                />,
            )
        }
        return (
            <div>
                {rows}
            </div>
        )
    }
}