import React from "react";
import {CellDTO, getSpreadsheet, listSpreadsheets, updateSpreadsheet} from "../api/spreadsheets";
import {SpreadsheetsSelectableList} from "./SpreadsheetsSelectableList";
import {Table} from "./Table";

interface WorkspaceState {
    spreadsheetIds: string[]
    selectedSpreadsheetId: string
    selectedSpreadsheetCells: Map<string, CellDTO>
}

export class Workspace extends React.Component<{}, WorkspaceState> {
    constructor(props: any) {
        super(props)
        this.setSelectedSpreadsheetId = this.setSelectedSpreadsheetId.bind(this)
        this.handleChangedCell = this.handleChangedCell.bind(this)
    }

    state = {
        spreadsheetIds: [],
        selectedSpreadsheetId: "",
        selectedSpreadsheetCells: new Map<string, CellDTO>()
    }

    componentDidMount() {
        listSpreadsheets().then((spreadsheets) => {
            this.setState({spreadsheetIds: spreadsheets})
        })
    }

    setSelectedSpreadsheetId = (event: React.ChangeEvent<HTMLSelectElement>) => {
        let selectedSpreadsheetId = event.target.value;
        this.setState({
            selectedSpreadsheetId: selectedSpreadsheetId,
        })
        console.log(`selected spreadsheet ID: ${selectedSpreadsheetId}`)
        console.log(`getting spreadsheet: ${selectedSpreadsheetId}`)
        getSpreadsheet(selectedSpreadsheetId).then((spreadsheet) => {
            console.log(`got spreadsheet: ${spreadsheet}`)
            this.setState({selectedSpreadsheetCells: spreadsheet})
        }).then(() => {
            console.log(this.state.selectedSpreadsheetCells)
        })
    }

    handleChangedCell = (id: string, expression: string) => {
        let updateSpreadsheetPayload = new Map<string, CellDTO>()
        if (expression.startsWith("=")) {
            updateSpreadsheetPayload.set(id, {value: "", expression: expression})
        } else {
            updateSpreadsheetPayload.set(id, {value: expression, expression: ""})
        }
        updateSpreadsheet(this.state.selectedSpreadsheetId, updateSpreadsheetPayload)
            .then((spreadsheet) => {
                console.log(`got spreadsheet: ${spreadsheet}`)
                this.setState({selectedSpreadsheetCells: spreadsheet})
            }).then(() => {
            console.log(this.state.selectedSpreadsheetCells)
        })
    }

    render() {
        return (
            <div style={{width: 'max-content'}}>
                <SpreadsheetsSelectableList
                    spreadsheetsIds={this.state.spreadsheetIds}
                    onSelectChange={this.setSelectedSpreadsheetId}
                />
                <Table numberOfColumns={10} numberOfRows={10} cells={this.state.selectedSpreadsheetCells}
                       handleChangedCell={this.handleChangedCell}/>
            </div>
        );
    }
}