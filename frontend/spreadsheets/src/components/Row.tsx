import React from "react";
import {Cell} from "./Cell";
import {CellDTO} from "../api/spreadsheets";

export const ALPHABET = ' abcdefghijklmnopqrstuvwxyz'.toUpperCase().split('');


type RowProps = {
    rowNo: number
    numberOfColumns: number
    handleChangedCell: Function,
    rowData: Map<string, CellDTO>
}

export const Row = (props: RowProps) => {
    const cells = []
    const y = props.rowNo
    for (let columnNo = 0; columnNo < props.numberOfColumns; columnNo += 1) {
        let cellKey = `${ALPHABET[columnNo]}${props.rowNo}`
        let cell = props.rowData.get(cellKey);
        let cellValue: string = ""
        let cellExpression: string = ""
        if (cell !== undefined) {
            cellValue = (cell.value || 0).toString() || ""
            cellExpression = cell.expression || ""
        }
        cells.push(
            <Cell
                key={cellKey}
                id={cellKey}
                y={y}
                x={columnNo}
                onChangedValue={props.handleChangedCell}
                value={cellValue}
                expression={cellExpression}
            />
        )
    }
    return (
        <div>
            {cells}
        </div>
    )
}
