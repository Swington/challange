const BACKEND_REST_API_URL = "https://r3nsohywub.execute-api.us-east-1.amazonaws.com/dev";

export type CellDTO = {
    value: string
    expression: string
}

const spreadsheetsUrl = `${BACKEND_REST_API_URL}/spreadsheets`;

export const createSpreadsheet = () =>
    fetch(spreadsheetsUrl, {method: 'post'})
        .then((response) => response.json())
        .then<Map<string, CellDTO>>((spreadsheet) => spreadsheet);

export const updateSpreadsheet = (id: string, payload: Map<string, CellDTO>) =>
    fetch(`${spreadsheetsUrl}/${id}`, {method: 'put', body: JSON.stringify(Object.fromEntries(payload))})
        .then((response) => response.json())
        .then<Map<string, CellDTO>>((spreadsheet) => spreadsheet);

export const listSpreadsheets = () =>
    fetch(spreadsheetsUrl)
        .then((response) => response.json())
        .then<string[]>((spreadsheets) => {
            return spreadsheets
        });

export const getSpreadsheet = (id: string) =>
    fetch(`${spreadsheetsUrl}/${id}`)
        .then((response) => {
            return response.json()
        })
        .then<Map<string, CellDTO>>((spreadsheet) => spreadsheet);
