import React from 'react';
import './App.css';
import {Workspace} from "./components/Workspace";

function App() {
    return (
        <div style={{width: 'max-content'}}>
            <Workspace/>
        </div>
    );
}

export default App;
