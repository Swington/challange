from __future__ import annotations

from dataclasses import dataclass, field
from decimal import Decimal
from typing import Dict, List, Set

AVAILABLE_OPERATORS = {
    "+",
    "-",
    "*",
    "/",
    "(",
    ")",
}

CURRENTLY_USED_FIELDS: Set[str] = set()


class CyclicReferenceError(Exception):
    def __init__(self, *args, **kwargs):
        super(CyclicReferenceError, self).__init__(*args, **kwargs)
        self.message = "cyclic reference error"


def raise_id_is_required_field_exception():
    raise Exception("id is required for spreadsheet field")


@dataclass
class SpreadsheetField:
    id: str = field(default_factory=raise_id_is_required_field_exception)
    value: str = "0"
    expression: str = ""

    def to_json(self):
        returned_dict = {}
        if self.value != Decimal(0):
            returned_dict['value'] = self.value
        if self.expression != "":
            returned_dict['expression'] = self.expression
        return returned_dict

    def get_value(self, other_fields: Dict[str, SpreadsheetField]) -> str:
        if not self.expression:
            return self.value
        else:
            return self.evaluate_expression(other_fields)

    def evaluate_expression(self, other_fields: Dict[str, SpreadsheetField]) -> str:
        if not self.expression or not self.expression.startswith("="):
            return "0"
        CURRENTLY_USED_FIELDS.add(self.id)
        evaluated_expression = self._evaluate_expression(self.expression, other_fields)
        CURRENTLY_USED_FIELDS.remove(self.id)
        return evaluated_expression

    @classmethod
    def _evaluate_expression(cls, expression: str, spreadsheet_fields: Dict[str, SpreadsheetField]) -> str:
        expression_list = cls._split_expression(expression)
        expression_to_evaluate: List[str] = []
        for part in expression_list:
            if part in AVAILABLE_OPERATORS:
                expression_to_evaluate.append(part)
                continue
            part_value = cls._get_expression_part_value(part, spreadsheet_fields)
            expression_to_evaluate.append(part_value)
        return str(eval("".join(expression_to_evaluate)))

    @classmethod
    def _get_expression_part_value(cls, part: str, spreadsheet_fields: Dict[str, SpreadsheetField]) -> str:
        try:
            int(part)
        except Exception:
            pass
        else:
            return part
        field = spreadsheet_fields.get(part.upper())
        if field is None:
            return "0"
        else:
            if field.id in CURRENTLY_USED_FIELDS:
                raise CyclicReferenceError
            return str(field.get_value(spreadsheet_fields))

    @staticmethod
    def _split_expression(expression: str) -> List[str]:
        expression = expression.split('=')[1]
        expression_list: List[str] = []
        current_field_id = ""
        for char in expression:
            if char in AVAILABLE_OPERATORS:
                if current_field_id:
                    expression_list.append(current_field_id)
                expression_list.append(char)
                current_field_id = ""
                continue
            current_field_id = current_field_id + char
        if current_field_id:
            expression_list.append(current_field_id)
        return expression_list
