import pytest

from spreadsheet_field import SpreadsheetField, CyclicReferenceError


@pytest.mark.parametrize(
    [
        "given_expression",
        "expected_expression_list",
    ],
    [
        (
                "=a1+c1",
                ["a1", "+", "c1"],
        ),
        (
                "=a1-c1",
                ["a1", "-", "c1"],
        ),
        (
                "=a1*c1",
                ["a1", "*", "c1"],
        ),
        (
                "=a1/c1",
                ["a1", "/", "c1"],
        ),
        (
                "=a1**c1",
                ["a1", "*", "*", "c1"],
        ),
        (
                "=a1+10",
                ["a1", "+", "10"],
        ),
        (
                "=(a1+10)",
                ["(", "a1", "+", "10", ")"],
        ),
    ]
)
def test_split_expression(given_expression, expected_expression_list):
    actual_expression_list = SpreadsheetField._split_expression(given_expression)

    assert actual_expression_list == expected_expression_list


@pytest.mark.parametrize(
    [
        "description",
        "given_field",
        "given_other_fields",
        "expected_value",
        "cyclic_reference",
    ],
    [
        (
                "getting empty field value",
                SpreadsheetField(id="A1"),
                {
                    "A1": SpreadsheetField(id="A1", value="10"),
                },
                "0",
                False,
        ),
        (
                "getting field value",
                SpreadsheetField(id="A1", value="10"),
                {
                    "A1": SpreadsheetField(id="A1", value="10"),
                },
                "10",
                False,
        ),
        (
                "getting field value when referenced in other fields expression",
                SpreadsheetField(id="A1", expression="=a2"),
                {
                    "A1": SpreadsheetField(id="A1", value="=a2"),
                    "A2": SpreadsheetField(id="A2", value="10"),
                },
                "10",
                False,
        ),
        (
                "getting field value when combining reference and value",
                SpreadsheetField(id="A1", expression="=a2+10"),
                {
                    "A1": SpreadsheetField(id="A1", value="=a2"),
                    "A2": SpreadsheetField(id="A2", value="10"),
                },
                "20",
                False,
        ),
        (
                "getting cyclic expression error when cyclic reference",
                SpreadsheetField(id="A1", expression="=a1"),
                {
                    "A1": SpreadsheetField(id="A1", value="=a1"),
                    "A2": SpreadsheetField(id="A2", value="10"),
                },
                "10",
                True,
        ),
    ]
)
def test_get_value(description, given_field, given_other_fields, expected_value, cyclic_reference):
    if cyclic_reference:
        with pytest.raises(CyclicReferenceError) as exc:
            given_field.get_value(given_other_fields)
    else:
        actual_value = given_field.get_value(given_other_fields)

        assert actual_value == expected_value
