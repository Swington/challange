import json
import os
import uuid

import boto3
import mock
import pytest
from moto import mock_dynamodb2

import spreadsheet_field


@pytest.fixture(scope='function')
def env():
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"
    os.environ["SPREADSHEETS_DYNAMODB_TABLE_NAME"] = "mock_spreadsheets"


@pytest.fixture
def app(mock_spreadsheets_table):
    import app
    return app


@pytest.fixture
def mock_spreadsheets_table(env):
    with mock_dynamodb2():
        dynamodb = boto3.resource("dynamodb")
        dynamodb.create_table(
            TableName=os.environ["SPREADSHEETS_DYNAMODB_TABLE_NAME"],
            AttributeDefinitions=[
                {
                    'AttributeName': 'id',
                    'AttributeType': 'S',
                }
            ],
            KeySchema=[
                {
                    'AttributeName': 'id',
                    'KeyType': 'HASH',
                },
            ],
        )
        table = dynamodb.Table(os.environ["SPREADSHEETS_DYNAMODB_TABLE_NAME"])
        table.put_item(Item={
            "id": "test-uuid4-0",
            "fields": {
                "A1": {
                    "value": "10",
                },
                "B1": {
                    "value": "10",
                    "expression": "=a1+c1",
                },
                "C1": {
                    "expression": "=d1+a1",
                },
            }
        })
        table.put_item(Item={
            "id": "test-uuid4-1",
            "fields": {
                "A1": {
                    "value": "21",
                    "expression": "=c1+d1",
                },
            },
        })
        yield table


def test_get_returns_single_spreadsheet_data(app):
    expected_response = {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(
            {
                "A1": {
                    "value": "10",
                },
                "B1": {
                    "value": "10",
                    "expression": "=a1+c1",
                },
                "C1": {
                    "value": "0",
                    "expression": "=d1+a1",
                },
            }),
    }

    actual_response = app.get({'pathParameters': {'spreadsheetId': "test-uuid4-0"}}, None)

    assert actual_response == expected_response


def test_list_returns_list_of_spreadsheets(app):
    expected_response = {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },

        "body": json.dumps(["test-uuid4-0", "test-uuid4-1"])
    }

    actual_response = app.list({}, None)

    assert actual_response == expected_response


def test_create_creates_new_spreadsheet(app):
    expected_response = {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps({'id': "test-uuid4"})
    }

    with mock.patch.object(uuid, 'uuid4', return_value="test-uuid4"):
        actual_response = app.post({}, None)

    assert actual_response == expected_response


@pytest.mark.parametrize(
    [
        "description",
        "given_update_body",
        "expected_response_body",
    ],
    [
        (
                "update existing field",
                {
                    "A1": {
                        "value": "11",
                    },
                },
                {
                    "A1": {
                        "value": "11",
                    },
                    "B1": {
                        "value": "22",
                        "expression": "=a1+c1",
                    },
                    "C1": {
                        "value": "11",
                        "expression": "=d1+a1",
                    },
                }
        ),
        (
                "remove existing field",
                {
                    "A1": {
                        "value": "",
                    },
                },
                {
                    "B1": {
                        "value": "0",
                        "expression": "=a1+c1",
                    },
                    "C1": {
                        "value": "0",
                        "expression": "=d1+a1",
                    },
                },
        ),
    ]
)
def test_update_updates_existing_spreadsheet_and_calculates_values(
        app,
        description,
        given_update_body,
        expected_response_body,
):
    expected_response = {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(expected_response_body)
    }

    actual_response = app.update({
        "body": json.dumps(given_update_body),
        "pathParameters": {
            "spreadsheetId": "test-uuid4-0",
        }
    },
        None,
    )

    assert actual_response == expected_response


@pytest.mark.parametrize(
    [
        'description',
        'given_spreadsheet_fields',
        'expected_spreadsheet_fields',
    ],
    [
        (
                "calculate values from expressions",
                {
                    "A1": {"id": "A1", "value": "10"},
                    "B1": {"id": "B1", "expression": "=a1+c1"},
                    "C1": {"id": "C1", "expression": "=d1+a1"},
                },
                {
                    "A1": {"id": "A1", "value": "10"},
                    "B1": {"id": "B1", "value": "20", "expression": "=a1+c1"},
                    "C1": {"id": "C1", "value": "10", "expression": "=d1+a1"},
                }
        ),
        (
                "calculate values from expression when referenced field has no value or doesn't exist",
                {
                    "A1": {"id": "A1"},
                    "B1": {"id": "B1", "expression": "=a1+c1"},
                },
                {
                    "A1": {"id": "A1"},
                    "B1": {"id": "B1", "value": "0", "expression": "=a1+c1"},
                }
        ),
        (
                "set error as field value when cyclic reference",
                {
                    "B1": {"id": "B1", "expression": "=a1+b1"},
                },
                {
                    "B1": {"id": "B1", "value": "cyclic reference error", "expression": "=a1+b1"},
                }
        ),
    ]
)
def test_calculate_values(app, description, given_spreadsheet_fields, expected_spreadsheet_fields):
    given_spreadsheet_fields = {key: spreadsheet_field.SpreadsheetField(**values) for key, values in
                                given_spreadsheet_fields.items()}
    expected_spreadsheet_fields = {key: spreadsheet_field.SpreadsheetField(**values) for key, values in
                                   expected_spreadsheet_fields.items()}

    actual_spreadsheet_fields = app._calculate_values(given_spreadsheet_fields)

    assert actual_spreadsheet_fields == expected_spreadsheet_fields
