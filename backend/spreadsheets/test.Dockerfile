FROM python:3.8-slim

ADD ./requirements .
RUN set -x && pip install -r requirements.dev.txt

ADD *.py tests/* /code/
WORKDIR /code
ENTRYPOINT ["python", "-m", "pytest", "-vv"]