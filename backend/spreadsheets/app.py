import json
import logging
import os
import uuid
from copy import deepcopy
from dataclasses import dataclass, field
from decimal import Decimal
from typing import Dict

import boto3
from boto3.dynamodb.conditions import Key

from spreadsheet_field import SpreadsheetField, CyclicReferenceError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

print('Loading function')

SPREADSHEETS_DYNAMODB_TABLE_NAME = os.getenv("SPREADSHEETS_DYNAMODB_TABLE_NAME")

dynamodb = boto3.resource("dynamodb")
spreadsheets_table = dynamodb.Table(SPREADSHEETS_DYNAMODB_TABLE_NAME)


@dataclass
class Spreadsheet:
    id: str
    fields: Dict[str, SpreadsheetField] = field(default_factory=dict)

    def to_json(self):
        return {
            "id": self.id,
            "fields": {
                id: data.to_json() for id, data in self.fields.items()
            }
        }


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return int(o)
        if isinstance(o, Spreadsheet):
            return o.to_json()
        return json.JSONEncoder.default(self, o)


def get(event, context):
    logger.info("Received event: " + json.dumps(event, indent=2))
    spreadsheet_id = event['pathParameters']['spreadsheetId']
    logger.info(f"getting spreadsheet with ID: {spreadsheet_id}")
    spreadsheet_dict = spreadsheets_table.query(KeyConditionExpression=Key('id').eq(spreadsheet_id))['Items'][0]
    logger.info(f"got spreadsheet: {spreadsheet_dict}")
    spreadsheet_fields = spreadsheet_dict.get('fields', {})
    spreadsheet = Spreadsheet(
        id=spreadsheet_dict['id'],
        fields={id: SpreadsheetField(id, **values) for id, values in spreadsheet_fields.items()}
    )
    output_data = {}
    for field_id, field_data in spreadsheet.fields.items():
        output_data[field_id] = {}
        if field_data.value:
            output_data[field_id]['value'] = field_data.value
        if field_data.expression:
            output_data[field_id]['expression'] = field_data.expression
    return {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(output_data, cls=CustomJSONEncoder)
    }


def list(event, context):
    logger.info("Received event: " + json.dumps(event, indent=2))
    spreadsheets = [spreadsheet['id'] for spreadsheet in spreadsheets_table.scan()["Items"]]
    return {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(spreadsheets, cls=CustomJSONEncoder)
    }


def post(event, context):
    logger.info("Received event: " + json.dumps(event, indent=2))
    generated_uuid4 = str(uuid.uuid4())
    spreadsheets_table.put_item(
        Item={
            "id": generated_uuid4,
        }
    )
    return {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps({
            "id": generated_uuid4,
        })
    }


def update(event, context):
    logger.info("Received event: " + json.dumps(event, indent=2))
    spreadsheet_id = event['pathParameters']['spreadsheetId']
    request_body = json.loads(event['body'])
    fields_to_update = {id: SpreadsheetField(id, **data) for id, data in request_body.items()}
    spreadsheet_dict = spreadsheets_table.query(KeyConditionExpression=Key('id').eq(spreadsheet_id))['Items'][0]
    spreadsheet_fields = spreadsheet_dict.get('fields', {})
    spreadsheet = Spreadsheet(
        id=spreadsheet_dict['id'],
        fields={id: SpreadsheetField(id, **values) for id, values in spreadsheet_fields.items()}
    )
    for field_id, field_data in fields_to_update.items():
        if field_data.value == "" and field_data.expression == "":
            if spreadsheet.fields.get(field_id):
                spreadsheet.fields.pop(field_id, None)
            continue
        spreadsheet.fields[field_id] = field_data
    updated_spreadsheet_fields = _calculate_values(spreadsheet.fields)
    updated_spreadsheet_fields_dict = {id: field.to_json() for id, field in updated_spreadsheet_fields.items()}
    spreadsheets_table.update_item(
        Key={
            "id": spreadsheet_id,
        },
        UpdateExpression="set #fs=:f",
        ExpressionAttributeValues={
            ":f": updated_spreadsheet_fields_dict,
        },
        ExpressionAttributeNames={
            "#fs": "fields",
        },
        ReturnValues="UPDATED_NEW",
    )
    return {
        "statusCode": 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(updated_spreadsheet_fields_dict, cls=CustomJSONEncoder)
    }


def _calculate_values(spreadsheet_fields: Dict[str, SpreadsheetField]) -> Dict[str, SpreadsheetField]:
    spreadsheet_fields_copy = deepcopy(spreadsheet_fields)
    for field_id, field_data in spreadsheet_fields_copy.items():
        if field_data.expression:
            try:
                field_value = field_data.evaluate_expression(spreadsheet_fields_copy)
            except CyclicReferenceError as e:
                spreadsheet_fields_copy[field_id].value = e.message
            else:
                spreadsheet_fields_copy[field_id].value = field_value
    return spreadsheet_fields_copy
