resource "aws_s3_bucket" "services_packages" {
  bucket = "${var.prefix}-services-packages"
  versioning {
    enabled = true
  }

  # TODO: Add bucket policy for minimal possible access
}
