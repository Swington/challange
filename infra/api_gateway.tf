resource "aws_api_gateway_rest_api" "challange" {
  name = "${var.env}-${var.prefix}-challange"
}

resource "aws_api_gateway_account" "challange" {
  cloudwatch_role_arn = aws_iam_role.api_gateway_cloudwatch.arn
}

resource "aws_iam_role" "api_gateway_cloudwatch" {
  name = "api_gateway_cloudwatch_global"

  assume_role_policy = data.aws_iam_policy_document.api_gateway_cloudwatch_assume_role.json
}

data "aws_iam_policy_document" "api_gateway_cloudwatch_assume_role" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"
      identifiers = [
        "apigateway.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role_policy" "api_gateway_cloudwatch" {
  name = "default"
  role = aws_iam_role.api_gateway_cloudwatch.id

  policy = data.aws_iam_policy_document.api_gateway_cloudwatch.json
}

data "aws_iam_policy_document" "api_gateway_cloudwatch" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_api_gateway_deployment" "dev" {
  depends_on = [
    module.get_spreadsheet,
    module.list_spreadsheets,
    module.create_spreadsheet,
    module.update_spreadsheet,
    module.spreadsheets_cors,
    module.spreadsheet_id_cors,
  ]

  rest_api_id = aws_api_gateway_rest_api.challange.id
  stage_name  = "dev"
}

resource "aws_api_gateway_method_settings" "dev_logging" {
  rest_api_id = aws_api_gateway_rest_api.challange.id
  stage_name  = "dev"
  method_path = "*/*"

  settings {
    data_trace_enabled = true
    logging_level      = "INFO"
  }
}
