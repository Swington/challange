resource "aws_s3_bucket" "frontend_bucket" {
  bucket = "${var.env}-${var.prefix}-frontend"

  versioning {
    enabled = true
  }

  policy = data.aws_iam_policy_document.frontend_bucket_policy.json

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  lifecycle_rule {
    prefix  = "/"
    enabled = true

    noncurrent_version_transition {
      days          = 30
      storage_class = "GLACIER"
    }
  }
}

data "aws_iam_policy_document" "frontend_bucket_policy" {
  statement {
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${var.env}-${var.prefix}-frontend/*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

