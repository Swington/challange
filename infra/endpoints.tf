resource "aws_api_gateway_resource" "spreadsheets" {
  rest_api_id = aws_api_gateway_rest_api.challange.id
  parent_id   = aws_api_gateway_rest_api.challange.root_resource_id
  path_part   = "spreadsheets"
}

module "spreadsheets_cors" {
  source = "squidfunk/api-gateway-enable-cors/aws"
  version = "0.3.1"

  api_id          = aws_api_gateway_rest_api.challange.id
  api_resource_id = aws_api_gateway_resource.spreadsheets.id
}

resource "aws_api_gateway_resource" "spreadsheet_id" {
  rest_api_id = aws_api_gateway_rest_api.challange.id
  parent_id   = aws_api_gateway_resource.spreadsheets.id
  path_part   = "{spreadsheetId}"
}

module "spreadsheet_id_cors" {
  source = "squidfunk/api-gateway-enable-cors/aws"
  version = "0.3.1"

  api_id          = aws_api_gateway_rest_api.challange.id
  api_resource_id = aws_api_gateway_resource.spreadsheet_id.id
}

# LIST spreadsheets

module "list_spreadsheets" {
  source = "./modules/api_gateway_lambda_endpoint"

  rest_api_id = aws_api_gateway_rest_api.challange.id
  resource_id = aws_api_gateway_resource.spreadsheets.id
  http_method = "GET"
  request_parameters = {
    "method.request.path.spreadsheetId" = true
  }

  lambda_name    = "${var.env}-${var.prefix}-list-spreadsheets"
  lambda_handler = "app.list"
  lambda_env_vars = {
    "SPREADSHEETS_DYNAMODB_TABLE_NAME" = aws_dynamodb_table.spreadsheets.name
  }
  dynamodb_table_arn = aws_dynamodb_table.spreadsheets.arn

  services_packages_bucket = aws_s3_bucket.services_packages.bucket
  service_package_key      = "spreadsheets.zip"
}

# GET spreadsheet
module "get_spreadsheet" {
  source = "./modules/api_gateway_lambda_endpoint"

  rest_api_id = aws_api_gateway_rest_api.challange.id
  resource_id = aws_api_gateway_resource.spreadsheet_id.id
  http_method = "GET"
  request_parameters = {
    "method.request.path.spreadsheetId" = true
  }

  lambda_name    = "${var.env}-${var.prefix}-get-spreadsheet"
  lambda_handler = "app.get"
  lambda_env_vars = {
    "SPREADSHEETS_DYNAMODB_TABLE_NAME" = aws_dynamodb_table.spreadsheets.name
  }
  dynamodb_table_arn = aws_dynamodb_table.spreadsheets.arn

  services_packages_bucket = aws_s3_bucket.services_packages.bucket
  service_package_key      = "spreadsheets.zip"
}

# CREATE spreadsheet
module "create_spreadsheet" {
  source = "./modules/api_gateway_lambda_endpoint"

  rest_api_id = aws_api_gateway_rest_api.challange.id
  resource_id = aws_api_gateway_resource.spreadsheets.id
  http_method = "POST"

  lambda_name    = "${var.env}-${var.prefix}-create-spreadsheet"
  lambda_handler = "app.post"
  lambda_env_vars = {
    "SPREADSHEETS_DYNAMODB_TABLE_NAME" = aws_dynamodb_table.spreadsheets.name
  }
  dynamodb_table_arn = aws_dynamodb_table.spreadsheets.arn

  services_packages_bucket = aws_s3_bucket.services_packages.bucket
  service_package_key      = "spreadsheets.zip"
}

# UPDATE spreadsheet
module "update_spreadsheet" {
  source = "./modules/api_gateway_lambda_endpoint"

  rest_api_id = aws_api_gateway_rest_api.challange.id
  resource_id = aws_api_gateway_resource.spreadsheet_id.id
  http_method = "PUT"
  request_parameters = {
    "method.request.path.spreadsheetId" = true
  }

  lambda_name    = "${var.env}-${var.prefix}-update-spreadsheet"
  lambda_handler = "app.update"
  lambda_env_vars = {
    "SPREADSHEETS_DYNAMODB_TABLE_NAME" = aws_dynamodb_table.spreadsheets.name
  }
  dynamodb_table_arn = aws_dynamodb_table.spreadsheets.arn

  services_packages_bucket = aws_s3_bucket.services_packages.bucket
  service_package_key      = "spreadsheets.zip"
}
