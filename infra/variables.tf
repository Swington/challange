variable "env" {
  type = string
  default = "tswiton"
}

variable "prefix" {
  type = string
  default = "cic"
}
