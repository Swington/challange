resource "aws_dynamodb_table" "spreadsheets" {
  name         = "${var.env}-${var.prefix}-spreadsheets"
  billing_mode = "PAY_PER_REQUEST"

  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}
