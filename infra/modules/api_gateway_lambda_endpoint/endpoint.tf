resource "aws_api_gateway_method" "lambda_method" {
  rest_api_id        = var.rest_api_id
  resource_id        = var.resource_id
  http_method        = var.http_method
  request_parameters = var.request_parameters
  authorization      = "NONE"
}

resource "aws_iam_role_policy_attachment" "api-lambda-invoker-role" {
  policy_arn = aws_iam_policy.lambda-invoke-policy.arn
  role       = aws_iam_role.lambda-invoker-role.name
}

resource "aws_api_gateway_integration" "get-job-status-integration" {
  rest_api_id = var.rest_api_id
  resource_id = var.resource_id
  http_method = var.http_method

  uri = aws_lambda_function.api_handler_lambda.invoke_arn

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
}

data "aws_iam_policy_document" "api-lambda-invoker-policy" {
  statement {
    effect = "Allow"
    actions = [
    "lambda:InvokeFunction"]
    resources = [
      aws_lambda_function.api_handler_lambda.arn
    ]
  }
}

resource "aws_iam_role" "lambda-invoker-role" {
  name               = "${var.lambda_name}InvokeAPIGatewayRole"
  assume_role_policy = data.aws_iam_policy_document.api-gateway-assume-role-policy.json
}

resource "aws_iam_policy" "lambda-invoke-policy" {
  name   = "${var.lambda_name}InvokeAPIGatewayPolicy"
  policy = data.aws_iam_policy_document.api-lambda-invoker-policy.json
}

data "aws_iam_policy_document" "api-gateway-assume-role-policy" {
  statement {
    effect = "Allow"
    actions = [
    "sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
      "apigateway.amazonaws.com"]
    }
  }
}
