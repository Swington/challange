# API Endpoint

variable "rest_api_id" {
  type = string
}

variable "resource_id" {
  type = string
}

variable "http_method" {
  type = string
}

variable "request_parameters" {
  type    = map
  default = null
}

# Lambda
variable "lambda_name" {
  type = string
}

variable "lambda_handler" {
  type = string
}

variable "services_packages_bucket" {
  type = string
}

variable "service_package_key" {
  type = string
}

variable "lambda_env_vars" {
  type = map
  default = null
}

variable "dynamodb_table_arn" {
  type = string
}
