resource "aws_lambda_function" "api_handler_lambda" {
  function_name     = var.lambda_name
  handler           = var.lambda_handler
  role              = aws_iam_role.lambda_service.arn
  runtime           = "python3.8"
  s3_bucket         = data.aws_s3_bucket_object.lambda_service_package.bucket
  s3_key            = data.aws_s3_bucket_object.lambda_service_package.key
  s3_object_version = data.aws_s3_bucket_object.lambda_service_package.version_id

  environment {
    variables = var.lambda_env_vars
  }
}

data "aws_s3_bucket_object" "lambda_service_package" {
  bucket = var.services_packages_bucket
  key    = var.service_package_key
}

resource "aws_iam_role" "lambda_service" {
  name               = "${var.lambda_name}_execution_role"
  assume_role_policy = data.aws_iam_policy_document.lambda_service_assume_role.json
}

resource "aws_lambda_permission" "api_gateway_invoke_lambda_permission" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api_handler_lambda.function_name
  principal     = "apigateway.amazonaws.com"
}

data "aws_iam_policy_document" "lambda_service_assume_role" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role_policy_attachment" "attach_basic_lambda_execution_role" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.lambda_service.name
}

resource "aws_iam_role_policy_attachment" "dynamodb_access" {
  policy_arn = aws_iam_policy.dynamodb_access.arn
  role       = aws_iam_role.lambda_service.name
}

resource "aws_iam_policy" "dynamodb_access" {
  name   = "${var.lambda_name}_dynadmodb_access"
  policy = data.aws_iam_policy_document.dynamodb_access.json
}

data "aws_iam_policy_document" "dynamodb_access" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:*" # TODO: should be changed to minimal required permissions
    ]
    resources = [
      var.dynamodb_table_arn,
      "${var.dynamodb_table_arn}/*",
    ]
  }
}
