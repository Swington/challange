# Project description/Decision log

## Repository structure
Repository is split into 3 main directories: backend, frontend and infra.
In a bigger project all 3 of those could potentially be stored in separate repositories.

## Backend 
Backend consists of services available behind REST API and a database.

### Services
Services are deployed using AWS Lambda, which means that they are serverless and managed by AWS.
Services are using Amazon DynamoDB as storage for spreadsheets data.

#### Packaging
Services artifacts, which are ZIP files, are stored as S3 Objects on a dedicated bucket.
Recently, AWS enabled to deploy Lambda functions utilizing Docker images. 
Those images, however contain whole logic responsible for running AWS Lambdas. It's by design, as those images can be run locally.
Unfortunately it means that compared to the size of ZIP files they have huge size. 
That's because, for the purposes of this project, I've utilized ZIP packages as Lambda functions artifacts.

To create Lambda package, run command:
```bash
make package
```
This command will create `spreadsheets.zip` file containing artifact from which all spreadsheets services are built.

To upload package to S3, run:
```bash
make upload
```

After uploading package to S3, one should run `terraform plan` and `apply` to update Lambda function and API Gateway configurations.

#### Working with the code locally
You can run unit tests by running command:
```bash
make test
```

Those tests will be run inside isolated environment of Docker container.


### REST API
REST API is implemented using AWS API Gateway. 
Requests sent to endpoints are forwarded to, and handled by services deployed on AWS Lambda. 
Utilizing API Gateway is a fully managed service.
This means there is less maintenance and configuration overhead for the developer regarding REST API, which is perfect for this particular task.

API is available on `https://r3nsohywub.execute-api.us-east-1.amazonaws.com/dev` and consists of 4 methods - `GET` and `POST` on `/spreadsheets` endpoint and `GET` and `PUT` on `/spreadsheets/{id}` endpoint. 
`POST` method on `/spreadsheets` endpoint does not need any payload body, and it returns `id` of the newly created spreadsheet, like so:
```json
{
  "id": "2a77a398-6449-4b30-aca1-9741be562d52"
}
```

`PUT` method on `/spreadsheets/{id}` endpoint is used to update existing spreadsheet. 

Example update payload:
```json
{
  "A1": {
    "value": "10"
  }
}
```

Example response for the above contains recalculated values of all spreadsheet cells, which hold some value.
So for the spreadsheet which before this update had a value `10` written into cell `B1`, the response for the above request would be. 
```json
{
  "A1": {
    "value": "10"
  },
  "B1": {
    "value": "20"
  }
}
```

You can try out this API using example Postman collection from file `challange.postman_collection.json`.

__KNOWN ISSUES/POSSIBLE IMPROVEMENTS__
* `DELETE` method should be added to `/spreadsheets/{id}` endpoint
* Ideally, `PATCH` method should be utilized here, as update consists only partial state of spreadsheet, but due to problems with API Gateway CORS integration with `PATCH` method, it was changed to `PUT`.
* OpenAPI documentation would be nice to have
* Ability to deploy changes locally could be added, for example utilizing `localstack`

### Database
Amazon DynamoDB is utilized as a persistent storage for this project.
As its NoSQL, it's easy to get started with it when writing code.
Because it's a fully managed service, there is less maintenance and configuration overhead.
Also, permissions of services regarding DynamoDB access can be configured using AWS IAM.

API Gateway with AWS Lambda because we want to use managed services to minimize maintenance overhead

## Frontend 
Frontend application allows user to select spreadsheet by its UUID and then view and manipulate its contents.
Spreadsheet changes persist in the backend database. 

Frontend application consists of files, which are statically hosted on S3 bucket.
Application itself had been implemented using TypeScript & React, and integrates with backend REST API for handling spreadsheet data.

### Working with the deployed application
Application is available under url: [`http://tswiton-cic-frontend.s3-website-us-east-1.amazonaws.com/`](http://tswiton-cic-frontend.s3-website-us-east-1.amazonaws.com/).

Before making changes to cells, please select existing spreadsheet.
Then you can write into cells upon clicking on them. 

__KNOWN ISSUES/POSSIBLE IMPROVEMENTS__
* Spreadsheet table should be visible only after choosing an spreadsheet
* Creating and deleting spreadsheets using frontend app would be nice
* Cyclic reference error should have better visibility
* Spreadsheet cells update could be better optimized - now whole table reloads when value of one cell is changed


### Working with the code locally
To build the application, run:
```bash
make build
```

To upload it to S3 bucket, run:
```bash
make upload
```

Now the built application is hosted on the S3 bucket.

## Infrastructure
Infrastructure of this project is deployed on AWS and is managed using IaaC.
Terraform is a tool of choice, because it enables for the project to remain quite open-minded about cloud provider choice.
If it was certain that this project would be deployed solely on AWS, one could use CloudFormation and/or its CDK to become more AWS-centric.

Terraform also simplifies handling IaaC of different system components.
Other potential candidates for deploying backend services and REST API would consist of, e.g. Serverless Framework.
I've chosen to use Terraform, because it gives more control over created resources and their configuration.
Also, managing relationships between resources created with different frameworks tend to overcomplicate CI/CD pipelines.

__KNOWN ISSUES/POSSIBLE IMPROVEMENTS__
* API Gateway deployment tends not update when REST API resources change 

## CI/CD Pipeline
Gitlab CI/CD was utilized for the purposes of this project.
Alternative would be GitHub Actions, which for the level of complexity of this particular project, would be a similarly adequate tool.

Current pipeline handles only terraform deployments.
Gitlab serves as Terraform state manager, which means that in order to run terraform commands for the project locally, one has to utilize terraform configuration state present in Gitlab.
If you wish to utilize terraform locally, please read on how to [configure backend](https://www.terraform.io/docs/language/settings/backends/configuration.html)
and [connect to it on Gitlab](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).

__TODO__:
* Handle backend and frontend services artifact update and deployment throughout environments
* Add more checks and utilize more static analysis tools for both backend and frontend services to monitor code quality and security

